
// Importing the dependencies/modules
const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoute = require('./routes/taskRoutes')

//Initialize dotenv
dotenv.config()

// Server setup 
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended:true}))

//MongoDB Connection

mongoose.connect(`mongodb+srv://mike18:${process.env.MONGODB_PASSWORD}@cluster0.hps6m9a.mongodb.net/S36-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection

db.on('error', () => console.error('Connection error.'))
db.on('open', () => console.log('We are connected to MongoDB!'))


// Routes
app.use('/tasks', taskRoute)


//Server listening

app.listen(port, () => console.log(`Server running on localhost:${port}`))
