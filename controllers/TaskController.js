const Task = require('../models/Task.js')

module.exports.createTask = (data) => {
	let newTask = new Task ({
		name:data.name
	})

	return newTask.save().then((savedTask, error) =>{
		if (error){
			console.log(error)
			return error
		}

		return savedTask
	})
}


module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result
	})
}


module.exports.updateTask = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error){
			console.log(error)
			return error
		}
		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}

module.exports.deleteTask = (task_id) => {
	return Task.findByIdAndDelete(task_id).then((removedTask, error) => {
			if (error){
				console.log(error)
				return error
			}

			return removedTask
		})
	}


	module.exports.getOne = (task_id) => {
		return Task.findOne(task_id).then((result, error) => {
		if (error){
				console.log(error)
				return error
			}


		console.log(result)
		return result
	})
}


module.exports.taskUpdate = (task_id, new_data) => {
	return Task.findById(task_id).then((result, error) => {
		if (error){
			console.log(error)
			return error
		}
		result.name = new_data.name

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error)
				return error
			}

			return updatedTask
		})
	})
}


